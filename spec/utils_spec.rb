# frozen_string_literal: true
require "./lib/utils"

describe Utils do
  describe ".format_currency" do
    it "returns pennies format when amount is below 100" do
      result = described_class.format_currency(50)

      expect(result).to eq("50p")
    end

    it "returns pound sterling format when value is equal or greater than 100" do
      result = described_class.format_currency(150)

      expect(result).to eq("£1.50")
    end
  end
end
