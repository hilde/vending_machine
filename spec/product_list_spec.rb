# frozen_string_literal: true
require "./lib/product_list"

describe ProductList do
  describe "#initialize" do
    it "initializes the object with an empty map of coins" do
      product_list = described_class.new

      expect(product_list.products).to eq({})
    end
  end

  describe "#load_product" do
    it "adds products if they are not already present" do
      product_list = described_class.new

      product_list.load_product(name: "Water", price: 100, quantity: 5)

      expect(product_list.products.size).to eq(1)
      expect(product_list.products.values.first).to eq(5)
      expect(product_list.products.keys.first).to be_instance_of(Product)
    end

    it "updates the product quantity if it is already present" do
      product_list = described_class.new
      product_list.load_product(name: "Water", price: 100, quantity: 5)
      product, current_quantity = product_list.products.first

      product_list.load_product(product: product, quantity: 5)

      expect(product_list.products.size).to eq(1)
      expect(product_list.products.values.first).to eq(10)
      expect(product_list.products.keys.first).to be_instance_of(Product)
    end
  end

  describe "#enough_quantity?" do
    it "returns true if the quantity of the product is above 0" do
      product_list = described_class.new
      product_list.load_product(name: "Water", price: 100, quantity: 5)
      product, current_quantity = product_list.products.first

      result = product_list.enough_quantity?(product: product)

      expect(result).to eq(true)
      expect(current_quantity).to eq(5)
    end

    it "returns false if the quantity of the product is 0" do
      product_list = described_class.new
      product_list.load_product(name: "Water", price: 100, quantity: 0)
      product, current_quantity = product_list.products.first

      result = product_list.enough_quantity?(product: product)

      expect(result).to eq(false)
      expect(current_quantity).to eq(0)
    end
  end

  describe "#deduct_product" do
    it "reduces the quantity of the products" do
      product_list = described_class.new
      product_list.load_product(name: "Water", price: 100, quantity: 5)
      product, _quantity = product_list.products.first

      product_list.deduct_product(product: product)

      expect(product_list.products[product]).to eq(4)
    end
  end

  describe "#setup" do
    it "creates 5 products" do
      product_list = described_class.new

      product_list.setup

      expect(product_list.products.size).to eq(5)
    end
  end
end
