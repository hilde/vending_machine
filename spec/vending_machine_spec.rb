# frozen_string_literal: true
require "./lib/vending_machine"

describe VendingMachine do
  describe "#initialize" do
    it "initializes the object for keeping track of the coins" do
      vending_machine = described_class.new

      expect(vending_machine.coin_box).to be_instance_of(CoinBox)
    end

    it "initializes the object for keeping track of the products" do
      vending_machine = described_class.new

      expect(vending_machine.product_list).to be_instance_of(ProductList)
    end

    it "loads the inventory of coins and products" do
      vending_machine = described_class.new

      expect(vending_machine.coin_box.coins).to_not eq({})
      expect(vending_machine.product_list.products).to_not eq({})
    end
  end

  describe "#denominations" do
    it "delegates the to coin_box" do
      vending_machine = described_class.new
      allow(vending_machine.coin_box).to receive(:available_denominations)

      vending_machine.denominations

      expect(vending_machine.coin_box).to have_received(:available_denominations)
    end
  end

  describe "#insert_coin" do
    it "adds instance of a coin to the vending machine" do
      coin = Coin.new(denomination: 50)
      vending_machine = described_class.new

      vending_machine.insert_coin(coin)

      expect(vending_machine.coins_inserted).to contain_exactly(coin)
    end
  end

  describe "#current_balance" do
    it "returns the sum of all coins inserted" do
      vending_machine = described_class.new
      vending_machine.insert_coin(Coin.new(denomination: 50))
      vending_machine.insert_coin(Coin.new(denomination: 100))
      vending_machine.insert_coin(Coin.new(denomination: 200))

      result = vending_machine.current_balance

      expect(result).to eq(350)
    end
  end

  describe "#products" do
    it "delegates the to product_list" do
      vending_machine = described_class.new
      allow(vending_machine.product_list).to receive(:products)

      vending_machine.products

      expect(vending_machine.product_list).to have_received(:products)
    end
  end

  describe "#select_product" do
    it "raises an exception if the product is sold out" do
      vending_machine = described_class.new
      product, current_quantity = vending_machine.products.find{ |(k, v)| v == 0 }

      expect {
        vending_machine.select_product(product)
       }.to raise_error(VendingMachine::ProductSoldOut)
    end

    it "sets the vending machine selected_product" do
      vending_machine = described_class.new
      product, current_quantity = vending_machine.products.find{ |(k, v)| v == 5 }

      vending_machine.select_product(product)

      expect(vending_machine.selected_product).to eq(product)
    end
  end

  describe "#buy" do
    it "raises an exception if the customer does not have enough cash" do
      vending_machine = described_class.new
      product, _quantity = vending_machine.products.find{ |(k, v)| v == 5 }
      vending_machine.select_product(product)

      expect {
        vending_machine.buy
       }.to raise_error(VendingMachine::NotEnoughBalance)
    end

    it "raises an exception if the vending machine does not have enough coins to give change" do
      vending_machine = described_class.new
      vending_machine.coin_box.coins.transform_values! {|v| 0 } # Deplete all coins

      product, _quantity = vending_machine.products.find{ |(product, _quantity)| product.name == "Pepsi" }
      vending_machine.insert_coin(Coin.new(denomination: 200))
      vending_machine.select_product(product)
      # Product cost is 100, we inserted 200, we expect 100, but there are no enough coins

      expect {
        vending_machine.buy
       }.to raise_error(VendingMachine::NotEnoughChange)
    end

    it "returns the change and updates the quantity of coins if we had to give it" do
      vending_machine = described_class.new
      product, _quantity = vending_machine.products.find{ |(product, _quantity)| product.name == "Pepsi" }
      vending_machine.insert_coin(Coin.new(denomination: 200))
      vending_machine.select_product(product)
      expect(vending_machine.coin_box.coins[200]).to eq(0)
      expect(vending_machine.coin_box.coins[100]).to eq(5)

      result = vending_machine.buy

      expect(vending_machine.coin_box.coins[200]).to eq(1)
      expect(vending_machine.coin_box.coins[100]).to eq(4)
      expect(result.map(&:denomination)).to match_array([100])
    end

    it "decreases the quantity of product selected" do
      vending_machine = described_class.new
      product, current_quantity = vending_machine.products.find{ |(product, _quantity)| product.name == "Coke" }
      vending_machine.insert_coin(Coin.new(denomination: 200))
      vending_machine.select_product(product)
      expect(current_quantity).to eq(5)

      vending_machine.buy

      _, new_quantity = vending_machine.products.find{ |(product, _quantity)| product.name == "Coke" }
      expect(new_quantity).to eq(current_quantity - 1)
    end
  end

  describe "#reset" do
    it "clears the internal state of the machine" do
      vending_machine = described_class.new
      coin = Coin.new(denomination: 200)
      product, current_quantity = vending_machine.products.find{ |(k, v)| k.name == "Coke" }
      coins_200 = vending_machine.coin_box.coins[200]
      vending_machine.insert_coin(coin)
      vending_machine.select_product(product)

      vending_machine.reset

      expect(vending_machine.selected_product).to be_nil
      expect(vending_machine.coins_inserted).to be_empty
    end
  end

  describe "#load_product" do
    it "add more products to the list, delegates to product_list" do
      product = double("Product")
      quantity = 5
      vending_machine = described_class.new
      allow(vending_machine.product_list).to receive(:load_product)

      vending_machine.load_product(product: product, quantity: 5)

      expect(vending_machine.product_list).to have_received(:load_product).with(product: product, quantity: 5)
    end
  end

  describe "#load_change" do
    it "add more coins to the box" do
      coin = double("Coin", denomination: 100)
      quantity = 5
      vending_machine = described_class.new
      allow(vending_machine.coin_box).to receive(:load_change)

      vending_machine.load_change(coin: coin, quantity: 5)

      expect(vending_machine.coin_box).to have_received(:load_change).with(denomination: 100, quantity: 5)
    end
  end
end
