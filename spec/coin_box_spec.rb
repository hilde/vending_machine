# frozen_string_literal: true
require "./lib/coin_box"
require "./lib/coin"

describe CoinBox do
  describe "#initialize" do
    it "initializes the object with an empty map of coins" do
      coin_box = described_class.new

      expect(coin_box.coins).to eq({})
    end
  end

  describe "#available_denominations" do
    it "returns an array of coins of the allowed denominations" do
      available_denominations = described_class.new.available_denominations

      expect(available_denominations.first).to be_instance_of(Coin)
      expect(available_denominations.size).to eq(8)
      expect(available_denominations.map(&:denomination)).to match_array([200, 100, 50, 20, 10, 5, 2, 1])
    end
  end

  describe "#load_change" do
    it "adds coins if they are not already present" do
      coin_box = described_class.new

      coin_box.load_change(denomination: 100, quantity: 5)

      expect(coin_box.coins.size).to eq(1)
      expect(coin_box.coins.values.first).to eq(5)
    end

    it "updates the coin quantity if it is already present" do
      coin_box = described_class.new
      coin_box.load_change(denomination: 100, quantity: 5)
      coin, current_quantity = coin_box.coins.first

      coin_box.load_change(denomination: 100, quantity: 5)

      expect(coin_box.coins.size).to eq(1)
      expect(coin_box.coins.values.first).to eq(10)
    end
  end

  describe "#register_sale" do
    it "increase the quantity for the denomination" do
      coin_box = described_class.new
      coin_box.load_change(denomination: 200, quantity: 0)
      coins_inserted = [Coin.new(denomination: 200)]

      coin_box.register_sale(coins_inserted: coins_inserted)

      expect(coin_box.coins[200]).to eq(1)
    end
  end

  describe "#available_coins" do
    it "return a list of available coins" do
      coin_box = described_class.new
      coin_box.load_change(denomination: 200, quantity: 5)
      coin_box.load_change(denomination: 50, quantity: 10)

      result = coin_box.available_coins

      expect(result.size).to eq(2)
      expect(result.first[0]).to be_instance_of(Coin)
      expect(result.first[1]).to eq(5)
    end
  end

  describe "#return_change" do
    it "returns nil when there are no coins to give change" do
      coin_box = described_class.new

      result = coin_box.return_change(100)

      expect(result).to be_nil
    end

    it "returns nil when there are no enough amount with coins to give change" do
      coin_box = described_class.new
      coin_box.load_change(denomination: 20, quantity: 1)
      coin_box.load_change(denomination: 50, quantity: 1)

      result = coin_box.return_change(100)

      expect(result).to be_nil
    end

    it "return a list of available coins with exact denomination" do
      coin_box = described_class.new
      coin_box.load_change(denomination: 100, quantity: 5)

      result = coin_box.return_change(100)

      expect(result.map(&:denomination)).to match_array([100])
    end

    it "return a list of available coins that sum the amount to change" do
      coin_box = described_class.new
      coin_box.load_change(denomination: 50, quantity: 1)
      coin_box.load_change(denomination: 50, quantity: 1)

      result = coin_box.return_change(100)

      expect(result.map(&:denomination)).to match_array([50, 50])
    end
  end

  describe "#deduct_change" do
    it "decreases the quantity for the denomination" do
      coin_box = described_class.new
      coin_box.load_change(denomination: 200, quantity: 5)
      change = [Coin.new(denomination: 200)]

      coin_box.deduct_change(change: change)

      expect(coin_box.coins[200]).to eq(4)
    end
  end

  describe "#setup" do
    it "creates 5 products" do
      coin_box = described_class.new

      coin_box.setup

      expect(coin_box.coins.size).to eq(8)
    end
  end
end
