# frozen_string_literal: true
require "./lib/coin"

describe Coin do
  describe "#initialize" do
    it "accepts the denomination when instantiating the object" do
      coin = described_class.new(denomination: 100)

      expect(coin.denomination).to eq(100)
    end

    it "raises an error if denomination is not in the allowed list" do
      expect {
        described_class.new(denomination: 55)
       }.to raise_error(ArgumentError, "Invalid denomination")
    end
  end

  describe "#to_s" do
    it "returns the pennies format when value is lower than 100" do
      coin = described_class.new(denomination: 20)

      expect(coin.to_s).to eq("20p")
    end

    it "returns the pound sterling format when value is equal or greater than 100" do
      coin = described_class.new(denomination: 200)

      expect(coin.to_s).to eq("£2")
    end
  end
end
