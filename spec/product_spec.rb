# frozen_string_literal: true
require "./lib/product"

describe Product do
  describe "#initialize" do
    it "accepts the name and price when instantiating the object" do
      product = described_class.new(name: "Water", price: 50)

      expect(product.name).to eq("Water")
      expect(product.price).to eq(50)
    end
  end

  describe "#formatted_price" do
    it "returns the formatted price when is lower than 100" do
      product = described_class.new(name: "Water", price: 50)

      expect(product.formatted_price).to eq("50p")
    end

    it "returns the formatted price when is equal or greater than 100" do
      product = described_class.new(name: "Water", price: 200)

      expect(product.formatted_price).to eq("£2.00")
    end
  end
end
