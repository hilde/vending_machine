# Vending Machine


## Exercise Task

Design a vending machine using a programming language of your choice. The vending machine should perform as follows:

-   Once an item is selected and the appropriate amount of money is inserted, the vending machine should return the correct product.
-   It should also return change if too much money is provided, or ask for more money if insufficient funds have been inserted.
-   The machine should take an initial load of products and change. The change will be in denominations of 1p, 2p, 5p, 10p, 20p, 50p, £1, £2. There should be a way of reloading either products or changes at a later point.
-   The machine should keep track of the products and change that it contains.

## Setup

You will need to have Ruby 2.7 or greater installed.

```bash
# Place yourself in the project folder
cd vending_machine

# Install dependencies
bundle install
```

### Execute the program
```bash
# To execute the script:
ruby prompt.rb
```

### How to run the test suite:

```bash
rspec .
```


## Entities

### Coin

Represents a coin, it has the responsibility of its formatting when printing, and validating the denomination.

### CoinBox

Represents the box in the machine with all coins, it has the responsibility of keeping track of the coins and their quantity, and calculating the change.

### Product

Represents a product, it has the responsibility of holding the name and it is price, and it formatting when printing.

### ProductList

Represents the inventory of products in the machine, it has the responsibility of keeping track of all the products and their quantity.

### VendingMachine

Represents the vending machine. It defines the public API that the interface will be interacting.

### Utils

Helper class, with the responsibility of holding non-business logic, for example, formatting currency.

## Interface

After executing the script you will see a screen like this:

```
❯ ruby prompt.rb
Select: (Press ↑/↓ arrow to move and Enter to select)
‣ Insert Coin
  Select Product
  Display Current Balance
  Ask Refund
  Maintenance: Load Products
  Maintenance: Load Change
  Maintenance: Report Inventory
  Quit
```
The first 4 options are self explanatory, the options with `Maintenance` are supposed to be run hidden from the customer, but for simplicity these are listed in the main menu.

I also have uploaded a video with a quick demo of how the script works at https://monosnap.com/file/CbFOGO6Qry8kC0Wp7CcXhOhdVjIZf3
Please note the video has audio but it's muted by default when the link opens.

## Products
When the vending machine is initialized is loaded with the following products:

Product Name | Price | Quantity
------------ | ----- | --------
Coke | 200 | 5
Pepsi | 100 | 5
Fanta | 100 | 5
Doritos | 50 | 5
Water | 20 | 0

## Coins
When the vending machine is initialized is loaded with the following coins:

Coin | Quantity
----- | --------
1p | 20
2p | 20
5p | 10
10p | 10
20p | 8
50p | 8
£1 | 5
£2 | 0
