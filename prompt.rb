# frozen_string_literal: true
require "tty-prompt"
require "./lib/vending_machine"
require "./lib/utils"

class Prompt
  attr_reader :prompt, :vending_machine

  def initialize
    @prompt = TTY::Prompt.new(quiet: true)
    @vending_machine = VendingMachine.new
  end

  def start
    loop do
      case main_menu
      when :insert_coin
        inserted_coin = coin_menu
        vending_machine.insert_coin(inserted_coin)
        prompt.ok("Inserted coin: #{inserted_coin}")
      when :select_product
        begin
          selected_product = products_menu

          vending_machine.select_product(selected_product)
          prompt.ok("Product Selected: #{vending_machine.selected_product.name} - Price: #{vending_machine.selected_product.formatted_price}")

          coins_change = vending_machine.buy
          prompt.ok("Here is your product: #{selected_product.name}, Enjoy!!")
          prompt.ok("Here is your change: #{format_coin_list(coins_change)}") if coins_change.any?

          vending_machine.reset
        rescue VendingMachine::ProductSoldOut
          prompt.error("Product Selected: #{selected_product.name} is unavailable. Please select other product.")
        rescue VendingMachine::NotEnoughBalance
          prompt.error("Your current balance #{Utils.format_currency(vending_machine.current_balance)} is too low")
        rescue VendingMachine::NotEnoughChange
          prompt.error("There is no enough change. Please try another product.")
        end
      when :balance
        prompt.ok("Current Balance: #{Utils.format_currency(vending_machine.current_balance)}")
      when :refund
        prompt.ok("Here are your coins back: #{format_coin_list(vending_machine.coins_inserted)}")
        vending_machine.reset
      when :load_product
        selected_product = products_menu
        quantity = prompt.ask("Indicate the quantity of products to add:", convert: :integer)

        vending_machine.load_product(product: selected_product, quantity: quantity)
      when :load_change
        selected_coin = coin_menu
        quantity = prompt.ask("Indicate the quantity of coins to add:", convert: :integer)

        vending_machine.load_change(coin: selected_coin, quantity: quantity)
      when :inventory
        prompt.ok("----- Products Report -----")
        vending_machine.product_list.products.each do |(product, quantity)|
          prompt.say("Product: #{product.name}, Quantity: #{quantity}")
        end
        prompt.ok("----- Coins Report -----")
        vending_machine.coin_box.available_coins.each do |(coin, quantity)|
          prompt.say("Coin: #{coin}, Quantity: #{quantity}")
        end
      when :quit
        break
      end
    end
  end

  private

  def main_menu
    prompt.select "Select:" do |menu|
      menu.per_page 8

      menu.choice "Insert Coin", :insert_coin
      menu.choice "Select Product", :select_product
      menu.choice "Display Current Balance", :balance
      menu.choice "Ask Refund", :refund
      menu.choice "Maintenance: Load Products", :load_product
      menu.choice "Maintenance: Load Change", :load_change
      menu.choice "Maintenance: Report Inventory", :inventory
      menu.choice "Quit", :quit
    end
  end

  def coin_menu
    prompt.select "Insert Coins:" do |menu|
      menu.per_page 8

      vending_machine.denominations.each do |coin|
        menu.choice coin.to_s, coin
      end
    end
  end

  def products_menu
    prompt.select "Select Product:" do |menu|
      vending_machine.products.each do |(product, quantity)|
        menu.choice "#{product.name} - Price: #{product.formatted_price} - Stock: #{quantity}", product
      end
    end
  end

  def format_coin_list(coin_list)
    coin_list.map(&:to_s).join(", ")
  end
end

Prompt.new.start
