# frozen_string_literal: true
require "./lib/utils"

class Product
  attr_reader :name, :price

  def initialize(name:, price:)
    @name, @price = name, price
  end

  def formatted_price
    Utils.format_currency(price)
  end
end
