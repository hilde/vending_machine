# frozen_string_literal: true

class Coin
  DENOMINATIONS = [200, 100, 50, 20, 10, 5, 2, 1]

  attr_reader :denomination

  def initialize(denomination:)
    raise ArgumentError.new("Invalid denomination") unless DENOMINATIONS.include?(denomination)

    @denomination = denomination
  end

  def to_s
    denomination < 100 ? "#{denomination}p" : "£#{denomination/100}"
  end
end
