# frozen_string_literal: true
require "./lib/coin_box"
require "./lib/product_list"

class VendingMachine
  attr_accessor :coin_box, :coins_inserted, :product_list, :selected_product

  ProductSoldOut = Class.new(StandardError)
  NotEnoughBalance = Class.new(StandardError)
  NotEnoughChange = Class.new(StandardError)

  def initialize
    @selected_product = nil
    @coins_inserted = []

    @coin_box = CoinBox.new
    @product_list = ProductList.new
    load_inventory
  end

  def denominations
    coin_box.available_denominations
  end

  def products
    product_list.products
  end

  def insert_coin(coin)
    coins_inserted << coin
  end

  def current_balance
    coins_inserted.map(&:denomination).reduce(0, &:+)
  end

  def select_product(product)
    raise ProductSoldOut unless product_list.enough_quantity?(product: product)

    @selected_product = product
  end

  def buy
    raise NotEnoughBalance unless enough_money?
    change = coin_box.return_change(change_ammount)

    raise NotEnoughChange if change.nil?

    coin_box.register_sale(coins_inserted: coins_inserted)
    coin_box.deduct_change(change: change)
    product_list.deduct_product(product: selected_product)

    change
  end

  def reset
    @selected_product = nil
    @coins_inserted = []
  end

  def load_product(product:, quantity:)
    product_list.load_product(product: product, quantity: quantity)
  end

  def load_change(coin:, quantity:)
    coin_box.load_change(denomination: coin.denomination, quantity: quantity)
  end

  private

  def enough_money?
    selected_product.price <= current_balance
  end

  def change_ammount
    current_balance - selected_product.price
  end

  def load_inventory
    product_list.setup
    coin_box.setup
  end
end
