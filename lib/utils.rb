# frozen_string_literal: true

class Utils
  def self.format_currency(amount)
    if amount < 100
      "#{amount}p"
    else
      "£#{Kernel.format('%0.2f', amount.to_f / 100)}"
    end
  end
end
