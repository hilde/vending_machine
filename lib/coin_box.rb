# frozen_string_literal: true
require "./lib/coin"

class CoinBox
  attr_reader :coins

  def initialize
    @coins = {}
  end

  def available_denominations
    Coin::DENOMINATIONS.map do |denomination|
      Coin.new(denomination: denomination)
    end
  end

  def load_change(denomination:, quantity:)
    coin = coins[denomination]

    if coin
      coins[denomination] += quantity
    else
      coins[denomination] = quantity
    end
  end

  def register_sale(coins_inserted:)
    coins_inserted.each do |coin|
      coins[coin.denomination] += 1
    end
  end

  def deduct_change(change:)
    change.each do |coin|
      coins[coin.denomination] -= 1
    end
  end

  def available_coins
    coins.transform_keys { |denomination| Coin.new(denomination: denomination) }
  end

  def return_change(amount)
    balance = amount
    result = []
    Coin::DENOMINATIONS.each do |denomination|
      next if denomination > balance

      quantity = coins[denomination]
      next unless(quantity && quantity > 0)

      coin_count = [balance / denomination, quantity].min

      same_denomination_coins = Array.new(coin_count){ denomination }
      result << same_denomination_coins
      balance -= same_denomination_coins.sum
    end

    return unless balance.zero?

    result.flatten.map do |denomination|
      Coin.new(denomination: denomination)
    end
  end

  def setup
    load_change(denomination: 1, quantity: 20)
    load_change(denomination: 2, quantity: 20)
    load_change(denomination: 5, quantity: 10)
    load_change(denomination: 10, quantity: 10)
    load_change(denomination: 20, quantity: 8)
    load_change(denomination: 50, quantity: 8)
    load_change(denomination: 100, quantity: 5)
    load_change(denomination: 200, quantity: 0)
  end
end
