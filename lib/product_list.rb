# frozen_string_literal: true
require "./lib/utils"
require "./lib/product"

class ProductList
  attr_reader :products

  def initialize
    @products = {}
  end

  def load_product(product: nil, name: nil, price: 0, quantity:)
    if products[product]
      products[product] += quantity
    else
      products[Product.new(name: name, price: price)] = quantity
    end
  end

  def enough_quantity?(product:)
    products[product] > 0
  end

  def deduct_product(product:)
    products[product] -= 1
  end

  def setup
    load_product(name: "Coke", price: 200, quantity: 5)
    load_product(name: "Pepsi", price: 100, quantity: 5)
    load_product(name: "Fanta", price: 100, quantity: 5)
    load_product(name: "Doritos", price: 50, quantity: 5)
    load_product(name: "Water", price: 20, quantity: 0)
  end
end
